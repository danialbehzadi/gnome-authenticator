gnome-authenticator (4.4.0-1) unstable; urgency=medium

  [ Danial Behzadi ]
  * New upstream version 4.4.0

 -- Danial Behzadi <dani.behzi@ubuntu.com>  Sat, 14 Oct 2023 14:26:22 +0330

gnome-authenticator (3.32.2+dfsg1-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Update lintian override info format in
    d/gnome-authenticator.lintian-overrides on line 2.
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 21 Nov 2022 18:27:53 +0000

gnome-authenticator (3.32.2+dfsg1-4) unstable; urgency=medium

  * d/patches: add 002_fix_meson_errors.patch to solve an FTBFS with meson 0.60
  * d/control: Set Standards-Version to 4.6.0 (no changes needed)

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Mon, 01 Nov 2021 10:15:11 +0000

gnome-authenticator (3.32.2+dfsg1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 21 Aug 2021 13:57:08 +0100

gnome-authenticator (3.32.2+dfsg1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Henry-Nicolas Tourneur ]
  * Upload to unstable
  * d/control: add missing dependencies on libhandy
  * d/tests: mark autopkgtest as superficial, allow-stderr and fix path issue
  * d/patches/000_handle_svg_removal.patch: apply DEP-3
  * d/patches/001_python38-cleanup.patch:
    prevent a startup crash with Python 3.8
  * Added a lintian override for the lack of manpage as this package is a gui

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Mon, 28 Sep 2020 16:54:14 +0000

gnome-authenticator (3.32.2+dfsg1-1) experimental; urgency=medium

  * Initial release (Closes: #958555)

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Tue, 02 Jun 2020 20:55:39 +0000
